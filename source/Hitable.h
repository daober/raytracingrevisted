#pragma once

#include "Ray.h"

class Material;


struct hit_rec {
    float t;
    Vector3 p;
    Vector3 normal;
    Material* mat_ptr;
};


class Hitable {
public:
    virtual bool hit( const Ray& r, float t_min, float t_max, hit_rec& rec ) const = 0;

};


class HitableList : public Hitable {
public:

    HitableList( ) {}
    HitableList( Hitable** l, int n ) { list = l; size = n; }

    virtual bool hit( const Ray& r, float t_min, float t_max, hit_rec& rec ) const {
        hit_rec temp_rec;
        bool hit_anything = false;
        double closest = t_max;

        for ( int i = 0; i < size; ++i ) {
            if ( list[i]->hit( r, t_min, closest, temp_rec ) ) {
                hit_anything = true;
                closest = temp_rec.t;
                rec = temp_rec;
            }
        }
        return hit_anything;
    }

private:

    Hitable** list;
    int size;
};