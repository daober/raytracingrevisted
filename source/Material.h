#pragma once
#include "Ray.h"
#include "Hitable.h"


class Material {
    public:
    virtual bool scatter( const Ray& ray_in, const hit_rec& rec, Vector3& attenuation, Ray& scattered) const = 0;
};